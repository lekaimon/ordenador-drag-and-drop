var objetosCarrito;
var carrito;
var total;
var componentes;
var componente = {
    img: '',
    precio: ''
};

function moverComp(event) {
    componente.img = event.target.children[0];
    componente.precio = event.target.children[1];
}


function borrarElementoCarrito(componenteAborrar){
    total.value = parseInt(total.value)-parseInt( componenteAborrar.children[1].value);
    componenteAborrar.remove(); 
}


function borrarElementoCarritoBoton(event){
    let componenteAborrar=event.target.parentNode;
    borrarElementoCarrito(componenteAborrar);

}


function borrarElementoCarritoArrastre(event){
    let componenteAborrar=event.target;
    borrarElementoCarrito(componenteAborrar);
}

function addCarrito() {
    
    total.value = parseFloat(total.value) + parseFloat(componente.precio.value);

    objetosCarrito=document.getElementById('componentesCarrito');


    let imagen=document.createElement('img')
    let precio=document.createElement('input');
    let contenedor=document.createElement('div');
    let idComp='comp'+objetosCarrito.childNodes.length;
    let borrar=document.createElement('button');
    
    
    imagen.src=componente.img.src;
    imagen.setAttribute('draggable',false);
    precio.value=componente.precio.value;


    contenedor.setAttribute('id',idComp);


    borrar.setAttribute('class','glyphicon glyphicon-trash');
    borrar.addEventListener('click',borrarElementoCarritoBoton);
    contenedor.appendChild(imagen);
    contenedor.appendChild(precio);
    contenedor.appendChild(borrar)
    contenedor.setAttribute('class','componente');
    contenedor.setAttribute('draggable', true);
    contenedor.addEventListener('dragend',borrarElementoCarritoArrastre);
    objetosCarrito.appendChild(contenedor);


}






function domCargado() {




    componentes = document.getElementsByClassName('componente');
    for (item of componentes) {
        
        item.addEventListener('dragstart', moverComp);
        
    }
    carrito = document.getElementById('carrito');
    total = document.getElementById('total');
    carrito.addEventListener('dragover', e => { e.preventDefault() });
    carrito.addEventListener('drop', addCarrito);

}



document.addEventListener('DOMContentLoaded', domCargado);